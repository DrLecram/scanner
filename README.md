# Dokumentenscanner

- Auslesen von Dokumenten Daten
- Nachbearbeitung der Daten
- Übernahme der Daten in eine Tabelle
- Löschen von erfassten Daten
- Bearbeiten von erfassten Daten
- Exportieren erfasster Daten als CSV Datei
- Backup Funktionalität via JSON Export / Import über das Menü

## ToDos

- Suchen in erfassten Daten
- Speicherung in Pinia Store
- Logo entwerfen + Logo Package erstellen für PWA
- PWA Analyse
- Seperates Speichern von Fotos
- Übertragen / Sync
